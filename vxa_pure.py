# Copyright (c) 2013-2014, mari_kiri
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies, 
# either expressed or implied, of whatever project this is.

from constants import *
import raw_pure
import ctypes
from pyglet.gl import *

class Chunk(raw_pure.Chunk):
	def draw(self):
		if USE_VBO and self.geom_vbo == None:
			self.geom_vbo = ctypes.c_uint(0)
			glGenBuffers(1, ctypes.byref(self.geom_vbo))
			if self.geom_vbo != None and self.geom_vbo != 0:
				print self.geom_vbo
				glBindBuffer(GL_ARRAY_BUFFER, self.geom_vbo)
				glBufferData(GL_ARRAY_BUFFER,
					self.geom[2]*(6+3),
					None,
					GL_STATIC_DRAW)
				glBufferSubData(GL_ARRAY_BUFFER,
					0,
					self.geom[2]*6,
					ctypes.byref(self.geom[0]))
				glBufferSubData(GL_ARRAY_BUFFER,
					self.geom[2]*6,
					self.geom[2]*3,
					ctypes.byref(self.geom[1]))
				glBindBuffer(GL_ARRAY_BUFFER, 0)

		if self.geom_vbo != None and self.geom_vbo != 0:
			glBindBuffer(GL_ARRAY_BUFFER, self.geom_vbo)
			glVertexPointer(3, GL_SHORT, 6, None)
			glColorPointer(3, GL_UNSIGNED_BYTE, 3, ctypes.c_void_p(self.geom[2]*6))
			glEnableClientState(GL_VERTEX_ARRAY)
			glEnableClientState(GL_COLOR_ARRAY)
			glDrawArrays(GL_QUADS, 0, self.geom[2])
			glDisableClientState(GL_COLOR_ARRAY)
			glDisableClientState(GL_VERTEX_ARRAY)
			glBindBuffer(GL_ARRAY_BUFFER, 0)
		else:
			glVertexPointer(3, GL_SHORT, 6, (self.geom[0]))
			glColorPointer(3, GL_UNSIGNED_BYTE, 3, (self.geom[1]))
			glEnableClientState(GL_VERTEX_ARRAY)
			glEnableClientState(GL_COLOR_ARRAY)
			glDrawArrays(GL_QUADS, 0, self.geom[2])
			glDisableClientState(GL_COLOR_ARRAY)
			glDisableClientState(GL_VERTEX_ARRAY)

	def update(self):
		if not self.dirty:
			return 0

		# We need separate colour and vertex data.
		geom_v = []
		geom_c = ""

		for sy in xrange(self.m.chunk_sz):
			for sx in xrange(self.m.chunk_sz):
				bx, by = self.cx + sx, self.cy + sy
				bx %= 512
				by %= 512

				(lh, lc) = self.m.data[by][bx]
				p = 0
				for (s, e, t) in lh:
					if t <= 2:
						geom_c += 4*lc[p:p+3]
						bz = s
						faces = FACE_DICT[F_TOP]
						for (x, y, z) in faces:
							geom_v.append(-(bx+x))
							geom_v.append(-(bz+y))
							geom_v.append(-(by+z))

					for bz in xrange(s, e, 1):
						geom_c += 16*lc[p:p+3]
						p += 3
						for f in (F_PX, F_NX, F_PY, F_NY):
							faces = FACE_DICT[f]
							for (x, y, z) in faces:
								geom_v.append(-(bx+x))
								geom_v.append(-(bz+y))
								geom_v.append(-(by+z))

					if t >= 2:
						bz = e-1
						geom_c += 4*lc[p-3:p]
						faces = FACE_DICT[F_BOTTOM]
						for (x, y, z) in faces:
							geom_v.append(-(bx+x))
							geom_v.append(-(bz+y))
							geom_v.append(-(by+z))

				assert p == len(lc)

		# All done!
		assert len(geom_v) == len(geom_c)
		self.geom = (
			(ctypes.c_short * len(geom_v))(*geom_v),
			(ctypes.c_ubyte * len(geom_c))(*(ord(c) for c in geom_c)),
			len(geom_v)//3
		)
		self.dirty = False
		return 1

	def get_faces(self):
		return (self.geom[2])//4

class Map(raw_pure.Map):
	chunk_loader = Chunk

	def load_vent(self, fp, x, y):
		# Alright. This one takes the VXL data,
		# and rearranges it for fast vertex array generation.
		if x == 0:
			print "%i/%i" % (y+1, 512)

		lh = []
		lc = ""
		(d, s, e, a) = (ord(c) for c in fp.read(4))
		while True:
			# Add the floor.
			if s <= e:
				lh.append((s, e+1, 2 if (d-1) == (e-s+1) else 1))

			# Stop if that's as far as we go.
			# Otherwise, continue.
			if d == 0:
				for i in xrange(e-s+1):
					lc += fp.read(3)[::-1]
					fp.read(1)
				break
			else:
				for i in xrange(d-1):
					lc += fp.read(3)[::-1]
					fp.read(1)

			# Get some new values.
			crun = (d-1)-(e-s+1)
			(d, s, e, a) = (ord(c) for c in fp.read(4))

			# Add the ceiling.
			if crun > 0:
				lh.append((a-crun, a, 3))

		return (lh, lc)

	def get_block_main(self, bx, by, bz):
		l = self.data[by][bx]
		p = 0
		while True:
			# Before the start, this is air.
			if bz < ord(l[p+1]):
				return None

			# Before and including the end, this is the visible floor.
			if bz <= ord(l[p+2]):
				ci = 4*(bz - ord(l[p+1]))
				return l[p+ci:p+ci+3]

			# If this is the end, it's hidden solid.
			if l[p] == "\x00":
				return True

			# There's only so much visible ceiling.
			# (D-1) - (E-S+1)
			crun = (ord(l[p])-1) - (ord(l[p+2])-ord(l[p+1])+1)

			# But we must move on to find out where it actually ends.
			p += 4*ord(l[p])

			# And now we can find out if we're in the hidden solid,
			if bz < ord(l[p+3]) - crun:
				return True

			# or in the visible ceiling.
			if bz < ord(l[p+3]):
				ci = 4*(bz - ord(l[p+3])) # This is not a mistake. We go backwards through the array.
				return l[p+ci:p+ci+3]

			# And once again, from the top.

	def set_block(self, bx, by, bz, v):
		bx %= 512
		by %= 512

		# TODO. This isn't actually going to work just yet.

		# Convert this and its neighbours to raw.
		l = self.to_raw(self, bx, by)
		lpx = self.to_raw(self, bx+1, by)
		lpy = self.to_raw(self, bx, by+1)
		lnx = self.to_raw(self, bx-1, by)
		lny = self.to_raw(self, bx, by-1)

		# Convert these back from raw.
		# Not done.

