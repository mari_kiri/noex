# Copyright (c) 2013-2014, mari_kiri
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies, 
# either expressed or implied, of whatever project this is.

from constants import *
import raw_pure

class Chunk(raw_pure.Chunk):
	def update_main(self):
		# Build some geometry.
		# This is actually really easy.
		# Especially if you're being lazy with the neighbouring cells.
		gbase = []

		for y in xrange(self.m.chunk_sz):
			for x in xrange(self.m.chunk_sz):
				bx, by = self.cx + x, self.cy + y
				bx %= 512
				by %= 512

				l = self.m.data[by][bx]
				p = 0
				while True:
					# This is the floor.
					s = ord(l[p+1])
					e = ord(l[p+2])
					if s <= e:
						f = p+4
						d = l[f:f+3]
						gbase.append((F_TOP, bx, by, s, d))
						for bz in xrange(s, e+1, 1):
							d = l[f:f+3]
							f += 4
							gbase.append((F_PX, bx, by, bz, d))
							gbase.append((F_NX, bx, by, bz, d))
							gbase.append((F_PY, bx, by, bz, d))
							gbase.append((F_NY, bx, by, bz, d))

					# Better stop if it's the last one!
					if l[p] == "\x00":
						break

					# And this is the ceiling!
					# (D-1) - (E-S+1)
					crun = (ord(l[p])-1) - (ord(l[p+2])-ord(l[p+1])+1)
					p += 4*ord(l[p])
					a = ord(l[p+3])

					f = p-4*crun
					for bz in xrange(a-crun, a, 1):
						d = l[f:f+3]
						f += 4
						gbase.append((F_PX, bx, by, bz, d))
						gbase.append((F_NX, bx, by, bz, d))
						gbase.append((F_PY, bx, by, bz, d))
						gbase.append((F_NY, bx, by, bz, d))
					d = l[p-4:p-4+3]
					gbase.append((F_BOTTOM, bx, by, a-1, d))

		return gbase

class Map(raw_pure.Map):
	chunk_loader = Chunk

	def load_vent(self, fp, x, y):
		# Now, this is a vxl loader which stores the pillars in VXL format.
		# It's a bit simpler, but at the cost of some of the other stuff being more complex.
		if x == 0:
			print "%i/%i" % (y+1, 512)

		l = ""
		while True:
			sa = fp.read(4)
			(d, s, e, a) = (ord(c) for c in sa)
			l += sa

			if d == 0:
				l += fp.read(4 * (e-s+1))
				break
			else:
				l += fp.read(4 * (d-1))

		return l

	def get_block_main(self, bx, by, bz):
		l = self.data[by][bx]
		p = 0
		while True:
			# Before the start, this is air.
			if bz < ord(l[p+1]):
				return None

			# Before and including the end, this is the visible floor.
			if bz <= ord(l[p+2]):
				ci = 4*(bz - ord(l[p+1]))
				return l[p+ci:p+ci+3]

			# If this is the end, it's hidden solid.
			if l[p] == "\x00":
				return True

			# There's only so much visible ceiling.
			# (D-1) - (E-S+1)
			crun = (ord(l[p])-1) - (ord(l[p+2])-ord(l[p+1])+1)

			# But we must move on to find out where it actually ends.
			p += 4*ord(l[p])

			# And now we can find out if we're in the hidden solid,
			if bz < ord(l[p+3]) - crun:
				return True

			# or in the visible ceiling.
			if bz < ord(l[p+3]):
				ci = 4*(bz - ord(l[p+3])) # This is not a mistake. We go backwards through the array.
				return l[p+ci:p+ci+3]

			# And once again, from the top.

	def to_raw(self, bx, by):
		bx %= 512
		by %= 512

		# A bit of creative copy-pasting here.
		sl = self.data[by][bx]
		p = 0

		l = [None for i in xrange(self.height)]
		(d, s, e, a) = (ord(c) for c in sl[p:p+4])
		p += 4
		z = 0
		while True:
			z = s
			runsz = e-s+1
			rund = d-1
			if d != 0 and runsz > rund:
				runsz = rund

			for i in xrange(runsz):
				l[z] = sl[p:p+4]
				p += 4
				rund -= 1
				z += 1

			if d == 0:
				break

			# If we didn't put the "tuple" thing in,
			# it would read AFTER we read the gap cell.
			# Which is not what we want.
			pq = tuple(sl[p+4*i:p+4*i+4] for i in xrange(rund))
			p += rund*4
			(nd, ns, ne, na) = (ord(c) for c in sl[p:p+4])
			p += 4

			while z < na - rund:
				l[z] = True
				z += 1
			for col in pq:
				l[z] = col
				z += 1

			d,s,e,a = nd,ns,ne,na

		while z < self.height:
			l[z] = True
			z += 1

		return l

	def from_raw(self, bx, by, l):
		bx %= 512
		by %= 512

		raise Exception("Now how does this go again?")

	def set_block(self, bx, by, bz, v):
		bx %= 512
		by %= 512

		# TODO. This isn't actually going to work just yet.

		# Convert this and its neighbours to raw.
		l = self.to_raw(bx, by)
		lpx = self.to_raw(bx+1, by)
		lpy = self.to_raw(bx, by+1)
		lnx = self.to_raw(bx-1, by)
		lny = self.to_raw(bx, by-1)

		# Set the relevant block.
		ov = l[bz]
		l[bz] = v

		# Convert these back from raw.
		self.from_raw(bx, by, l)
		self.from_raw(bx+1, by, lpx)
		self.from_raw(bx, by+1, lpy)
		self.from_raw(bx-1, by, lnx)
		self.from_raw(bx, by-1, lny)

