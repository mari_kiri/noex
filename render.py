# Copyright (c) 2013-2014, mari_kiri
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies, 
# either expressed or implied, of whatever project this is.

import ctypes
import math
import pyglet
from pyglet.gl import *
from pyglet.window import key
from constants import *

import octree

SHADER_VERTEX = """
#version 120
varying vec3 norm;
varying vec3 wpos;
varying float amb;

attribute float ambient;

void main()
{
	amb = ambient;
	norm = gl_Normal.xyz;
	wpos = vec3(gl_ModelViewMatrix * gl_Vertex);
	gl_Position = ftransform();
	//gl_FrontColor = vec4(1.0);
	gl_FrontColor = gl_Color;
}
"""

SHADER_FRAGMENT = """
#version 120
varying vec3 norm;
varying vec3 wpos;
varying float amb;

//uniform vec2 oc_start;
//uniform sampler2D oc_tex;

uniform vec3 test_light;
//const vec3 test_light = normalize(vec3(0.5, -1, 0.3));
const vec4 fog_color = vec4(0.0, 0.3, 0.0, 1.0);
const float fog_dist = 20000.0; // 63% falloff point

void main()
{
	float dist = length(wpos);
	float fog_level = exp(-dist/fog_dist);
	float d = max(0.0, -dot(test_light, normalize(norm)));

	float a = 0.3;
	//d *= (1.0 - a);
	a *= amb;
	gl_FragColor = (gl_Color * (d + a)) * fog_level
		+ fog_color * (1.0 - fog_level);
}
"""

SHADER_VERTEX_PREFBO = """
#version 120
varying float face;

void main()
{
	vec3 norm = normalize(gl_Normal.xyz);
	face = 0.0;
	/**/ if(norm.x <= -0.5) face = 0.0;
	else if(norm.x >= +0.5) face = 0.2;
	else if(norm.y <= -0.5) face = 0.4;
	else if(norm.y >= +0.5) face = 0.6;
	else if(norm.z <= -0.5) face = 0.8;
	else if(norm.z >= +0.5) face = 1.0;
	gl_Position = ftransform();
	gl_FrontColor = vec4(gl_Color.rgb, face);
}
"""

SHADER_FRAGMENT_PREFBO = """
#version 120
varying float face;

void main()
{
	gl_FragColor = gl_Color;
}
"""

SHADER_VERTEX_POSTFBO = """
#version 120
varying vec2 tcsrc;

void main()
{
	tcsrc = gl_MultiTexCoord0.xy;
	gl_Position = gl_Vertex;
}
"""

SHADER_FRAGMENT_POSTFBO = """
#version 120
varying vec2 tcsrc;
uniform sampler2D tex;
uniform sampler2D tex_depth;

vec3 get_norm(float face)
{
	vec3 norm;

	if(face < 0.5)
	{
		/**/ if(face < 0.1) norm = vec3(-1.0,  0.0,  0.0);
		else if(face < 0.3) norm = vec3(+1.0,  0.0,  0.0);
		else /************/ norm = vec3( 0.0, -1.0,  0.0);
	} else {
		/**/ if(face < 0.7) norm = vec3( 0.0, +1.0,  0.0);
		else if(face < 0.9) norm = vec3( 0.0,  0.0, -1.0);
		else /************/ norm = vec3( 0.0,  0.0, +1.0);
	}
	
	return norm;
}

vec4 get_pos(vec2 src)
{
	// We need the z,w 2x2 from the projection matrix
	vec4 zsrc = gl_ProjectionMatrix * vec4(0.0, 0.0, 1.0, 0.0);
	vec4 wsrc = gl_ProjectionMatrix * vec4(0.0, 0.0, 0.0, 1.0);

	float a = zsrc.z;
	float b = wsrc.z;
	float c = zsrc.w;
	float d = wsrc.w;
	
	float q = texture2D(tex_depth, src).x;

	float z = -(q*d - b) / (a - q*c);
	return vec4(src*2.0-1.0, z, 1.0);
}

vec4 get_pos_world(vec2 src)
{
	return gl_ModelViewMatrixInverse * get_pos(src);
}

const vec3 light_dir = normalize(vec3(0.5, 1.0, 0.3));

void main()
{
	vec2 tc = tcsrc;
	const vec2 toffs = vec2(1.0) / vec2(800.0, 600.0);
	vec4 pos = get_pos(tc);
	/*
	tc = pos.xy * pos.z / 80.0;
	tc = tc*0.5+0.5;
	pos = get_pos(tc);
	tc = pos.xy * pos.z / 80.0;
	tc = tc*0.5+0.5;
	pos = get_pos(tc);
	*/
	vec4 color = texture2D(tex, tc);
	float face = color.a;
	vec3 gnorm = get_norm(color.a);
	vec3 norm = vec3(gl_ModelViewMatrixInverse * vec4(gnorm, 0.0));
	vec3 light_local = vec3(gl_ModelViewMatrixInverse * vec4(light_dir, 0.0));
	vec2 boffs = toffs / (pos.z / 60.0);
	vec2 qoffs = toffs * (pos.z / 400.0);

	vec4 b0 = color;
	vec4 b1 = texture2D(tex, tc + qoffs * vec2( 2.0, 0.0)) - b0;
	vec4 b2 = texture2D(tex, tc + qoffs * vec2( 1.0, 0.0)) - b0;
	vec4 b3 = texture2D(tex, tc + qoffs * vec2(-1.0, 0.0)) - b0;
	vec4 b4 = texture2D(tex, tc + qoffs * vec2(-2.0, 0.0)) - b0;
	
	vec4 bz = (b1 + b2 + b3 + b4);
	color = b0 + bz / 4.0;

	vec4 c1 = texture2D(tex, tc + boffs * vec2( 2.0, 0.0));
	vec4 c2 = texture2D(tex, tc + boffs * vec2(-2.0, 0.0));
	vec4 c3 = texture2D(tex, tc + boffs * vec2( 0.0, 2.0));
	vec4 c4 = texture2D(tex, tc + boffs * vec2( 0.0,-2.0));
	vec4 c5 = texture2D(tex, tc + boffs * vec2( 1.0, 1.0));
	vec4 c6 = texture2D(tex, tc + boffs * vec2(-1.0, 1.0));
	vec4 c7 = texture2D(tex, tc + boffs * vec2( 1.0,-1.0));
	vec4 c8 = texture2D(tex, tc + boffs * vec2(-1.0,-1.0));
	vec4 ca = texture2D(tex, tc + boffs * vec2( 1.0, 0.0));
	vec4 cb = texture2D(tex, tc + boffs * vec2(-1.0, 0.0));
	vec4 cc = texture2D(tex, tc + boffs * vec2( 0.0, 1.0));
	vec4 cd = texture2D(tex, tc + boffs * vec2( 0.0,-1.0));

	float f1 = c1.a;
	float f2 = c2.a;
	float f3 = c3.a;
	float f4 = c4.a;
	float f5 = c5.a;
	float f6 = c6.a;
	float f7 = c7.a;
	float f8 = c8.a;
	float fa = ca.a;
	float fb = cb.a;
	float fc = cc.a;
	float fd = cd.a;

	vec4 p1 = get_pos(tc + boffs * vec2( 2.0, 0.0));
	vec4 p2 = get_pos(tc + boffs * vec2(-2.0, 0.0));
	vec4 p3 = get_pos(tc + boffs * vec2( 0.0, 2.0));
	vec4 p4 = get_pos(tc + boffs * vec2( 0.0,-2.0));
	vec4 p5 = get_pos(tc + boffs * vec2( 1.0, 1.0));
	vec4 p6 = get_pos(tc + boffs * vec2(-1.0, 1.0));
	vec4 p7 = get_pos(tc + boffs * vec2( 1.0,-1.0));
	vec4 p8 = get_pos(tc + boffs * vec2(-1.0,-1.0));
	vec4 pa = get_pos(tc + boffs * vec2( 1.0, 0.0));
	vec4 pb = get_pos(tc + boffs * vec2(-1.0, 0.0));
	vec4 pc = get_pos(tc + boffs * vec2( 0.0, 1.0));
	vec4 pd = get_pos(tc + boffs * vec2( 0.0,-1.0));

	float lightaff = max(0.0, min(1.0, 1.0 - pos.z/150.0));
	float amp_lightaff = min(1.0, lightaff * 7.0);
	if(f1 != face || f2 != face || f3 != face || f4 != face
	|| f5 != face || f6 != face || f7 != face || f8 != face
	|| fa != face || fb != face || fc != face || fd != face
	|| abs(dot((p1 - pos).xyz, norm)) >= 0.5
	|| abs(dot((p5 - pos).xyz, norm)) >= 0.5
	|| abs(dot((p3 - pos).xyz, norm)) >= 0.5
	|| abs(dot((p4 - pos).xyz, norm)) >= 0.5
	|| abs(dot((p5 - pos).xyz, norm)) >= 0.5
	|| abs(dot((p6 - pos).xyz, norm)) >= 0.5
	|| abs(dot((p7 - pos).xyz, norm)) >= 0.5
	|| abs(dot((p8 - pos).xyz, norm)) >= 0.5
	|| abs(dot((pa - pos).xyz, norm)) >= 0.5
	|| abs(dot((pb - pos).xyz, norm)) >= 0.5
	|| abs(dot((pc - pos).xyz, norm)) >= 0.5
	|| abs(dot((pd - pos).xyz, norm)) >= 0.5
			)
		color.rgb *= (1.0 - amp_lightaff) + 0.1 * amp_lightaff;
	
/*
	int i;
	float offsmul = 0.025;
	float fogdist = pos.z;
	for(i = 1; i <= 16; i++)
	{
		float offs = offsmul * float(i);
		float sh1 = get_pos(tc + light_local.xy * offs).z;
		if(sh1 < pos.z + offs)
		{
			color.rgb *= 0.5;
			fogdist -= 250.0;
			break;
		}
	}
	fogdist = max(0.0, fogdist - 50.0);
	color.rgb += fogdist/300.0;
*/

	color *= lightaff * (0.3 + 0.7 * max(0.0, dot(gnorm, light_dir)))
		+ (1.0 - lightaff);
	//color *= min(1.0, (pos.z * pos.z));
	//gl_FragColor = vec4(vec3(0.0, 0.0, 1.0) * zdist/500.0 + color.rgb * (1.0 - zdist/500.0), 1.0);
	//gl_FragColor = vec4(vec3(zdist/500.0) , 1.0);
	gl_FragColor = vec4(color.rgb, 1.0);
	//gl_FragColor = vec4(vec3(length(pos) / 150.0), 1.0);
	//gl_FragColor = vec4(norm*0.5+0.5, 1.0);
}
"""


class Renderer:
	def __init__(self, m):
		self.m = m

		self.dir_x = 0
		self.dir_y = 0
		self.dir_z = 0
		self.rdir_x = 0
		self.rdir_y = 0

		self.pos_x = -256
		self.pos_y = 0
		self.pos_z = -256
		self.rpos_x = 90 * math.pi / 180.0
		self.rpos_y = 0

	def make_shaders(self):
		self.fbo = ctypes.c_uint(0)
		glGenFramebuffers(1, ctypes.byref(self.fbo))
		print "FBO", self.fbo
		glBindFramebuffer(GL_FRAMEBUFFER, self.fbo)
		self.ftex_color = ctypes.c_uint(0)
		self.ftex_depth = ctypes.c_uint(0)

		glEnable(GL_TEXTURE_2D)
		glGenTextures(1, ctypes.byref(self.ftex_color))
		glGenTextures(1, ctypes.byref(self.ftex_depth))

		glBindTexture(GL_TEXTURE_2D, self.ftex_color)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 800, 600, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, None)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			self.ftex_color, 0)

		glBindTexture(GL_TEXTURE_2D, self.ftex_depth)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, 800, 600, 0,
			GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, None)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
			self.ftex_depth, 0)

		glBindTexture(GL_TEXTURE_2D, 0)


		shader_v = glCreateShader(GL_VERTEX_SHADER)
		shader_f = glCreateShader(GL_FRAGMENT_SHADER)
		src_v = (ctypes.POINTER(ctypes.c_char)*1)(ctypes.create_string_buffer(SHADER_VERTEX_PREFBO))
		src_f = (ctypes.POINTER(ctypes.c_char)*1)(ctypes.create_string_buffer(SHADER_FRAGMENT_PREFBO))
		glShaderSource(shader_v, 1, src_v, None)
		glShaderSource(shader_f, 1, src_f, None)
		glCompileShader(shader_v)
		glCompileShader(shader_f)
		self.shader = glCreateProgram()
		glAttachShader(self.shader, shader_v)
		glAttachShader(self.shader, shader_f)
		#glBindAttribLocation(self.shader, 1, "ambient")
		glLinkProgram(self.shader)
		#glUseProgram(self.shader)

		shader_v = glCreateShader(GL_VERTEX_SHADER)
		shader_f = glCreateShader(GL_FRAGMENT_SHADER)
		src_v = (ctypes.POINTER(ctypes.c_char)*1)(ctypes.create_string_buffer(SHADER_VERTEX_POSTFBO))
		src_f = (ctypes.POINTER(ctypes.c_char)*1)(ctypes.create_string_buffer(SHADER_FRAGMENT_POSTFBO))
		glShaderSource(shader_v, 1, src_v, None)
		glShaderSource(shader_f, 1, src_f, None)
		glCompileShader(shader_v)
		glCompileShader(shader_f)
		self.shader_post = glCreateProgram()
		glAttachShader(self.shader_post, shader_v)
		glAttachShader(self.shader_post, shader_f)
		glLinkProgram(self.shader_post)
		#glUseProgram(self.shader_post)

		if False:
			# Make an octree.
			self.oc_texdata, self.oc_start, self.oc_w, self.oc_h = octree.calc(self.m)

			# Load that octree thing we just made.
			glEnable(GL_TEXTURE_2D)
			self.oc_tex = ctypes.c_uint(0)
			glGenTextures(1, ctypes.byref(self.oc_tex))
			glBindTexture(GL_TEXTURE_2D, self.oc_tex)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, self.oc_w, self.oc_h, 0,
				GL_RGBA, GL_UNSIGNED_BYTE, ctypes.byref(
					ctypes.create_string_buffer(self.oc_texdata)))
			glBindTexture(GL_TEXTURE_2D, 0)

	def prepare_pyglet(self):
		#config = pyglet.gl.Config()
		self.window = pyglet.window.Window(
			#config=config,
			width=800, height=600,
			#width=1366, height=768,
			vsync=False)
		self.sun_time = 0.0

		@self.window.event
		def on_draw():
			# We need depth testing.
			glEnable(GL_DEPTH_TEST)
			glEnable(GL_CULL_FACE)

			# What pyglet gives us is stupid.
			# We want a standard projection matrix.
			glMatrixMode(GL_PROJECTION)
			glLoadIdentity()
			gluPerspective(90, self.window.width / float(self.window.height), .1, 400)

			if REVERSE_DEPTH:
				glClearDepth(0);
				glDepthFunc(GL_GEQUAL);
			else:
				glClearDepth(1);
				glDepthFunc(GL_LEQUAL);

			# A camera would be nice here.
			glMatrixMode(GL_MODELVIEW)
			glLoadIdentity()
			glRotatef(self.rpos_x * 180 / math.pi, 1, 0, 0)
			glRotatef(self.rpos_y * 180 / math.pi, 0, 1, 0)
			glTranslatef(-self.pos_x, -self.pos_y, -self.pos_z)

			glClearColor(0, 0.3, 0, 0)
			glBindFramebuffer(GL_FRAMEBUFFER, self.fbo)
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

			# Render some chunks.
			glUseProgram(self.shader)
			glUniform3f(glGetUniformLocation(self.shader, "test_light"),
				math.sin(self.sun_time*0.03)*math.cos(self.sun_time*0.3),
				-math.sin(self.sun_time*0.3),
				math.cos(self.sun_time*0.3)*math.cos(self.sun_time*0.03))
			if hasattr(self.m, "draw"):
				# The other way gets a bit leaky.
				#print "Calling 'draw'"
				self.m.draw(-self.pos_x, -self.pos_z, 512)
				pass
			else:
				new_chunks_remain = 1
				l = self.m.chunks_in_range(-self.pos_x, -self.pos_z, 512)
				l.sort() # The C renderers don't sort their lists.
				for (_, c, _, _) in l:
					if new_chunks_remain > 0 or not c.dirty:
						new_chunks_remain -= c.update()
						c.draw()
						pass

			# Perform postproc step
			glUseProgram(self.shader_post)
			glBindFramebuffer(GL_FRAMEBUFFER, 0)
			glFlush()

			glDisable(GL_CULL_FACE)
			glDisable(GL_DEPTH_TEST)
			glEnable(GL_TEXTURE_2D)
			glActiveTexture(GL_TEXTURE0)
			glBindTexture(GL_TEXTURE_2D, self.ftex_color)
			glActiveTexture(GL_TEXTURE1)
			glBindTexture(GL_TEXTURE_2D, self.ftex_depth)
			glActiveTexture(GL_TEXTURE0)
			glUniform1i(glGetUniformLocation(self.shader_post, "tex"), 0)
			glUniform1i(glGetUniformLocation(self.shader_post, "tex_depth"), 1)
			glBegin(GL_QUADS)
			if True:
				glTexCoord2f( 0.0,  0.0)
				glVertex2f(-1.0, -1.0)
				glTexCoord2f( 1.0,  0.0)
				glVertex2f( 1.0, -1.0)
				glTexCoord2f( 1.0,  1.0)
				glVertex2f( 1.0,  1.0)
				glTexCoord2f( 0.0,  1.0)
				glVertex2f(-1.0,  1.0)
			glEnd()
			print "FPS:", pyglet.clock.get_fps()
		
		@self.window.event
		def on_key_press(sym, mods):
			global REVERSE_DEPTH
			if   sym == key.W: self.dir_z = -1
			elif sym == key.S: self.dir_z =  1
			elif sym == key.A: self.dir_x = -1
			elif sym == key.D: self.dir_x =  1
			elif sym == key.LCTRL: self.dir_y = -1
			elif sym == key.SPACE: self.dir_y =  1
			elif sym == key.LEFT:  self.rdir_y = -1
			elif sym == key.RIGHT: self.rdir_y =  1
			elif sym == key.UP:    self.rdir_x = -1
			elif sym == key.DOWN:  self.rdir_x =  1

			elif sym == key.BACKSLASH: REVERSE_DEPTH = not REVERSE_DEPTH

		@self.window.event
		def on_key_release(sym, mods):
			if   sym == key.W: self.dir_z = -0
			elif sym == key.S: self.dir_z =  0
			elif sym == key.A: self.dir_x = -0
			elif sym == key.D: self.dir_x =  0
			elif sym == key.LCTRL: self.dir_y = -0
			elif sym == key.SPACE: self.dir_y =  0
			elif sym == key.LEFT:  self.rdir_y = -0
			elif sym == key.RIGHT: self.rdir_y =  0
			elif sym == key.UP:    self.rdir_x = -0
			elif sym == key.DOWN:  self.rdir_x =  0

		def on_update(dt):
			rot_speed = 1.3 * math.pi * dt
			move_speed = 30 * dt
			self.rpos_y += rot_speed * self.rdir_y
			self.rpos_x += rot_speed * self.rdir_x

			dx_z = move_speed * -math.sin(self.rpos_y) * math.cos(self.rpos_x)
			dy_z = move_speed * math.sin(self.rpos_x)
			dz_z = move_speed * math.cos(self.rpos_y) * math.cos(self.rpos_x)

			dx_x = move_speed * math.cos(self.rpos_y)
			dy_x = move_speed * 0
			dz_x = move_speed * math.sin(self.rpos_y)

			dx_y = move_speed * math.sin(self.rpos_x) * math.sin(self.rpos_y)
			dy_y = move_speed * math.cos(self.rpos_x)
			dz_y = move_speed * math.sin(self.rpos_x) * -math.cos(self.rpos_y)

			self.pos_x += dx_x * self.dir_x + dx_z * self.dir_z + dx_y * self.dir_y
			self.pos_y += dy_x * self.dir_x + dy_z * self.dir_z + dy_y * self.dir_y
			self.pos_z += dz_x * self.dir_x + dz_z * self.dir_z + dz_y * self.dir_y

			self.sun_time += dt

		self.make_shaders()

		pyglet.clock.schedule_interval(on_update, 1/60.0)
		#pyglet.clock.schedule_interval(on_update, 1/200.0)

	def run(self):
		self.prepare_pyglet()
		pyglet.app.run()

def start(m):
	Renderer(m).run()

