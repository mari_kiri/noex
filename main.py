#!/usr/bin/env python2

# The chunk loader in Ace of Spades 1.x is a bit slow.
# Let's see if we can do better.

# Copyright (c) 2013-2014, mari_kiri
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies, 
# either expressed or implied, of whatever project this is.

MODE = "render"
#MODE = "octree"

from constants import *

import sys
import time

import raw_pure
import vxl_pure
import vxl_ctypes
import vxa_pure
import vxb_pure

# You'll need to compile these.
import raw_c

import render
import octree

# "map" is the name of a useful Python function.
# Don't call anything "map".

m = None
if sys.argv[1] == "-raw":
	m = raw_pure.Map(sys.argv[2])
elif sys.argv[1] == "-vxl":
	m = vxl_pure.Map(sys.argv[2])
elif sys.argv[1] == "-vxa":
	m = vxa_pure.Map(sys.argv[2])
elif sys.argv[1] == "-rawc":
	m = raw_c.Map(sys.argv[2], CHUNK_SIZE)
else:
	raise Exception("I don't think that's a valid argument.")

if MODE == "load_all":
	tlist = []
	for cy in xrange(512//m.chunk_sz):
		for cx in xrange(512//m.chunk_sz):
			tstart = time.time()
			m.get_chunk(cx, cy).update()
			tend = time.time()
			faces = m.get_chunk(cx, cy).get_faces()
			tlist.append((tend - tstart, faces))
			print cx, cy, faces, tend - tstart

	print tlist
	tlist_times = [a for (a,b) in tlist]
	tlist_times_nonzero = [a for (a,b) in tlist if b != 0]
	if len(tlist_times_nonzero) == 0:
		tlist_times_nonzero = [0]
	print min(tlist_times_nonzero), max(tlist_times), sum(tlist_times)/len(tlist_times), sum(tlist_times_nonzero)/len(tlist_times_nonzero)

	"""
	These were done at 1.6GHz while compiling LibreOffice at the same time.
	The memory usage statistics were done on completely separate runs.
	Because I am totally getting paid to do this.

	CityOfChicago.vxl:
	- Raw, pure, 16: 0.0515220165253   0.546591997147 0.0992409314495 0.111908065672  (700MB loaded, 1161MB at end)
	- VxA, pure, 16: 0.000756025314331 0.388691902161 0.0260343817063 0.0376322317466
	- VxA, pure, 16: 0.000459909439087 0.188101053238 0.0142905525863 0.0205343321931 (while building ATLAS)
	- VxB, pure, 16: 0.000616073608398 0.206849098206 0.0131846773438 0.0189707605101 (while building ATLAS)
	- VXL, pure, 16: 0.000824213027954 1.91709804535  0.0472751799971 0.0692673676306 (125MB loaded, 933MB at end)
	- VXL, pure, 16: 0.000947952270508 1.1679828167   0.0344805573113 0.0504746498821 (while building ATLAS)
	- VXL, ctyp, 16: 0.00141286849976  1.70729804039  0.0707849387545 0.103657013735

	London.vxl:
	- Raw, pure, 16: 0.0605120658875   1.39908099174  0.0866200793535 0.116563095563
	- VxA, pure, 16: 0.00115084648132  0.551239013672 0.0142386977095 0.0401829774829
	- VXL, pure, 16: 0.00219392776489  0.609392881393 0.0294037188869 0.0852353082187
	- VXL, ctyp, 16: 0.00221490859985  1.38872885704  0.0324021258857 0.0934173024219
	"""
elif MODE == "render":
	render.start(m)
elif MODE == "octree":
	octree.calc(m)

