# Copyright (c) 2013-2014, mari_kiri
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies, 
# either expressed or implied, of whatever project this is.

from constants import *
import raw_pure
import vxl_pure

class Chunk(raw_pure.Chunk):
	def update(self):
		if not self.dirty:
			return 0

		# We need separate colour and vertex data.
		geom_v = []
		geom_c = ""

		for y in xrange(self.m.chunk_sz):
			for x in xrange(self.m.chunk_sz):
				bx, by = self.cx + x, self.cy + y
				bx %= 512
				by %= 512

				(lh, lc) = self.m.data[by][bx]
				geom_c += lc
				p = 0
				for (s, e, t) in lh:
					if t <= 2:
						bz = s
						faces = FACE_DICT[F_TOP]
						for (x, y, z) in faces:
							geom_v.append(  bx+x )
							geom_v.append(-(bz+y))
							geom_v.append(-(by+z))

					for bz in xrange(s, e, 1):
						for f in (F_PX, F_NX, F_PY, F_NY):
							faces = FACE_DICT[f]
							for (x, y, z) in faces:
								geom_v.append(  bx+x )
								geom_v.append(-(bz+y))
								geom_v.append(-(by+z))

					if t >= 2:
						bz = e-1
						faces = FACE_DICT[F_BOTTOM]
						for (x, y, z) in faces:
							geom_v.append(  bx+x )
							geom_v.append(-(bz+y))
							geom_v.append(-(by+z))

		# All done!
		# Set glVertexPointer and glColorPointer to suit.
		self.geom = (geom_v, geom_c)
		self.dirty = False
		return 1

	def get_faces(self):
		return len(self.geom[0])//12

class Map(vxl_pure.Map):
	chunk_loader = Chunk

	def load_vent(self, fp, x, y):
		# Alright. This one takes the VXL data,
		# and rearranges it for fast vertex array generation.
		if x == 0:
			print "%i/%i" % (y+1, 512)

		lh = []
		lc = ""
		(d, s, e, a) = (ord(c) for c in fp.read(4))
		while True:
			# Add the floor.
			if s <= e:
				lh.append((s, e+1, 2 if (d-1) == (e-s+1) else 1))

			# Stop if that's as far as we go.
			# Otherwise, continue.
			if d == 0:
				for i in xrange(e-s+1):
					lc += fp.read(3)*(20 if i == 0 and s <= e else 16)
					fp.read(1)
				break
			else:
				for i in xrange(d-1):
					lc += fp.read(3)*(20 if i == 0 and s <= e else 16)
					fp.read(1)

			# Get some new values.
			crun = (d-1)-(e-s+1)
			(d, s, e, a) = (ord(c) for c in fp.read(4))

			# Add the ceiling.
			if crun > 0:
				lh.append((a-crun, a, 3))

			lc += lc[-12:]

		return (lh, lc)

	def get_block_main(self, bx, by, bz):
		# Well, this managed to be simpler than the vxl version.
		l = self.data[by][bx]
		p = 0
		for (s, e, t) in l[0]:
			if bz < s:
				return None if t <= 2 else True

			if bz < e:
				b = 12 + 48*(bz-s)
				return l[1][p+b: p+b+3]

			p += 48*(e-s)
			if t <= 2: p += 12
			if t >= 2: p += 12

		return True

	def from_raw(self, bx, by, sl):
		raise Exception("Well, I haven't worked this out just yet.")

	def to_raw(self, bx, by):
		bx %= 512
		by %= 512
		sl = self.data[by][bx]

		l = [None for i in xrange(self.height)]
		bz = 0
		p = 0
		for (s, e, t) in sl[0]:
			if t > 2:
				while bz <= s:
					l[bz] = True
					bz += 1
			else:
				p += 12

			for z in xrange(s, e, 1):
				l[bz] = sl[1][p:p+3]
				p += 48

			bz = e

