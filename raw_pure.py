# Copyright (c) 2013-2014, mari_kiri
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies, 
# either expressed or implied, of whatever project this is.

from constants import *
import ctypes
from pyglet.gl import *

class Chunk:
	def __init__(self, m, cx, cy):
		self.m = m
		self.cx, self.cy = cx, cy
		self.dirty = True
		self.geom = None
		self.geom_vbo = None

	def mark(self):
		# Any time this chunk is modified, this needs to be called.
		# This will not be a problem if you call Map.set_block().
		self.dirty = True

	def draw(self):
		if USE_VBO and self.geom_vbo == None:
			self.geom_vbo = ctypes.c_uint(0)
			glGenBuffers(1, ctypes.byref(self.geom_vbo))
			if self.geom_vbo != None and self.geom_vbo != 0:
				print self.geom_vbo
				glBindBuffer(GL_ARRAY_BUFFER, self.geom_vbo)
				glBufferData(GL_ARRAY_BUFFER,
					len(self.geom)*4,
					self.geom,
					GL_STATIC_DRAW)
				glBindBuffer(GL_ARRAY_BUFFER, 0)

		if self.geom_vbo != None and self.geom_vbo != 0:
			glBindBuffer(GL_ARRAY_BUFFER, self.geom_vbo)
			glVertexPointer(3, GL_SHORT, 12, None)
			glColorPointer(3, GL_SHORT, 12, ctypes.c_void_p(6))
			glEnableClientState(GL_VERTEX_ARRAY)
			glEnableClientState(GL_COLOR_ARRAY)
			glDrawArrays(GL_QUADS, 0, len(self.geom)//6)
			glDisableClientState(GL_COLOR_ARRAY)
			glDisableClientState(GL_VERTEX_ARRAY)
			glBindBuffer(GL_ARRAY_BUFFER, 0)
		else:
			glVertexPointer(3, GL_SHORT, 12, (self.geom))
			glColorPointer(3, GL_SHORT, 12, ctypes.byref(self.geom, 6))
			glEnableClientState(GL_VERTEX_ARRAY)
			glEnableClientState(GL_COLOR_ARRAY)
			glDrawArrays(GL_QUADS, 0, len(self.geom)//6)
			glDisableClientState(GL_COLOR_ARRAY)
			glDisableClientState(GL_VERTEX_ARRAY)

	def update_main(self):
		# Build some geometry.
		gbase = []

		for y in xrange(self.m.chunk_sz):
			for x in xrange(self.m.chunk_sz):
				bx, by = self.cx + x, self.cy + y
				bx %= 512
				by %= 512
				l = self.m.data[by][bx]
				lpx = self.m.data[by][(bx+1) % 512]
				lnx = self.m.data[by][(bx-1) % 512]
				lpy = self.m.data[(by+1) % 512][bx]
				lny = self.m.data[(by-1) % 512][bx]

				last_data = None
				for bz in xrange(len(l)):
					d = l[bz]
					if d != True and d != None:
						# This is a colour block.
						if last_data == True or last_data == None:
							gbase.append((F_TOP, bx, by, bz, d))

						if lpx[bz] == True or lpx[bz] == None:
							gbase.append((F_PX, bx, by, bz, d))
						if lnx[bz] == True or lnx[bz] == None:
							gbase.append((F_NX, bx, by, bz, d))
						if lpy[bz] == True or lpy[bz] == None:
							gbase.append((F_PY, bx, by, bz, d))
						if lny[bz] == True or lny[bz] == None:
							gbase.append((F_NY, bx, by, bz, d))

					elif last_data != True and last_data != None:
						gbase.append((F_BOTTOM, bx, by, bz-1, last_data))

					last_data = d

		return gbase

	def get_faces(self):
		return len(self.geom)//24

	def update(self):
		if not self.dirty:
			return 0

		gbase = self.update_main()

		# Like, actually build geometry now.
		geom = [0 for i in xrange(len(gbase) * 4 * (3 + 3))]
		p = 0
		for (dir, bx, by, bz, d) in gbase:
			faces = FACE_DICT[dir]
			for (x, y, z) in faces:
				geom[p + 0] = -(bx+x)
				geom[p + 1] = -(bz+y)
				geom[p + 2] = -(by+z)
				geom[p + 3] = ord(d[2])*32767//255
				geom[p + 4] = ord(d[1])*32767//255
				geom[p + 5] = ord(d[0])*32767//255
				p += 6

		# All done!
		self.geom = (ctypes.c_short * len(geom))(*geom)
		self.dirty = False
		return 1

class Map:
	chunk_loader = Chunk

	def __init__(self, fname, height=242):
		# If it were up to me, I'd just store the vxl data as-is, without unpacking it.
		# But that would be an unfair comparison.
		self.height = height
		self.load_from_fname(fname)
		self.chunk_sz = CHUNK_SIZE

		assert 512 % self.chunk_sz == 0, "Let's be realistic. That chunk size just isn't going to cut the mustard."
		self.chunks = [[None for x in xrange(512 // self.chunk_sz)] for y in xrange(512 // self.chunk_sz)]

	def load_from_fname(self, fname):
		fp = open(fname, "rb")
		r = self.load_from_fp(fp)
		fp.close()
		return r

	def chunks_in_range(self, px, py, r):
		cx1 = int(px-r-1)//self.chunk_sz
		cy1 = int(py-r-1)//self.chunk_sz
		cx2 = int(px+r+1)//self.chunk_sz
		cy2 = int(py+r+1)//self.chunk_sz

		cx1 = max(0, min(512//self.chunk_sz-1, cx1))
		cy1 = max(0, min(512//self.chunk_sz-1, cy1))
		cx2 = max(0, min(512//self.chunk_sz-1, cx2))
		cy2 = max(0, min(512//self.chunk_sz-1, cy2))

		l = []

		for y in xrange(cy1, cy2+1, 1):
			for x in xrange(cx1, cx2+1, 1):
				d = (x*self.chunk_sz+self.chunk_sz//2-px)**2 + (y*self.chunk_sz+self.chunk_sz//2-py)**2
				if d < (r+self.chunk_sz*1.5)**2:
					c = self.get_chunk(x, y)
					l.append((d, c, x, y))

		l.sort()

		return l

	def load_from_fp(self, fp):
		self.data = [[self.load_vent(fp, x, y) for x in xrange(512)] for y in xrange(512)]

	def load_vent(self, fp, x, y):
		# OK. This shouldn't need explanation.
		# It's a vxl loader which stores the map in a raw [y][x][z] format.
		# Exactly how this is done is not important - all I need is a 512x512xH raw map.
		if x == 0:
			print "%i/%i" % (y+1, 512)

		l = [None for i in xrange(self.height)]
		(d, s, e, a) = (ord(c) for c in fp.read(4))
		#print d,s,e,a
		z = 0
		while True:
			z = s
			runsz = e-s+1
			rund = d-1
			if d != 0 and runsz > rund:
				runsz = rund

			for i in xrange(runsz):
				l[z] = fp.read(4)
				rund -= 1
				z += 1

			if d == 0:
				break

			# If we didn't put the "tuple" thing in,
			# it would read AFTER we read the gap cell.
			# Which is not what we want.
			pq = tuple(fp.read(4) for i in xrange(rund))
			(nd, ns, ne, na) = (ord(c) for c in fp.read(4))
			#print nd,ns,ne,na

			while z < na - rund:
				l[z] = True
				z += 1
			for col in pq:
				l[z] = col
				z += 1

			d,s,e,a = nd,ns,ne,na

		while z < self.height:
			l[z] = True
			z += 1

		return l

	def get_block(self, bx, by, bz):
		bx %= 512
		by %= 512

		if bz < 0:
			return None
		elif bz >= self.height:
			return True
		else:
			return self.get_block_main(bx, by, bz)

	def get_block_main(self, bx, by, bz):
		return self.data[by][bx][bz]

	def set_block(self, bx, by, bz, v):
		bx %= 512
		by %= 512
		self.data[by][bx][bz] = v

		# Update this chunk.
		cx = bx//self.chunk_sz
		cy = by//self.chunk_sz
		sx = bx%self.chunk_sz
		sy = by%self.chunk_sz
		self.get_chunk(cx, cy).mark()

		# Update any neighbours, too!
		if sx == 0: self.get_chunk(cx-1, cy).mark()
		if sy == 0: self.get_chunk(cx, cy-1).mark()
		if sx == self.chunk_sz-1: self.get_chunk(cx+1, cy).mark()
		if sy == self.chunk_sz-1: self.get_chunk(cx, cy+1).mark()

	def get_chunk(self, cx, cy):
		cx %= 512//self.chunk_sz
		cy %= 512//self.chunk_sz
		cnk = self.chunks[cy][cx] or self.chunk_loader(self, cx*self.chunk_sz, cy*self.chunk_sz)
		self.chunks[cy][cx] = cnk
		return cnk

