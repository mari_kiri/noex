# Copyright (c) 2013-2014, mari_kiri
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies, 
# either expressed or implied, of whatever project this is.

from ctypes import *

from constants import *
import raw_pure

class Chunk(raw_pure.Chunk):
	def update_main(self):
		# Build some geometry.
		# This is actually really easy.
		# Especially if you're being lazy with the neighbouring cells.
		gbase = []

		for y in xrange(self.m.chunk_sz):
			for x in xrange(self.m.chunk_sz):
				bx, by = self.cx + x, self.cy + y
				bx %= 512
				by %= 512

				l = self.m.data[by][bx]
				p = 0
				while True:
					# This is the floor.
					s = l[p+1]
					e = l[p+2]
					if s <= e:
						f = p+4
						d = l[f:f+3]
						gbase.append((F_TOP, bx, by, s, d))
						for bz in xrange(s, e+1, 1):
							d = l[f:f+3]
							f += 4
							gbase.append((F_PX, bx, by, bz, d))
							gbase.append((F_NX, bx, by, bz, d))
							gbase.append((F_PY, bx, by, bz, d))
							gbase.append((F_NY, bx, by, bz, d))

					# Better stop if it's the last one!
					if l[p] == 0:
						break

					# And this is the ceiling!
					# (D-1) - (E-S+1)
					crun = (int(l[p]-1) - (int(l[p+2])-int(l[p+1])+1))
					p += 4*int(l[p])
					a = l[p+3]

					f = p-4*crun
					for bz in xrange(a-crun, a, 1):
						d = l[f:f+3]
						f += 4
						gbase.append((F_PX, bx, by, bz, d))
						gbase.append((F_NX, bx, by, bz, d))
						gbase.append((F_PY, bx, by, bz, d))
						gbase.append((F_NY, bx, by, bz, d))
					d = l[p-4:p-4+3]
					gbase.append((F_BOTTOM, bx, by, a-1, d))

		return gbase

	def update(self):
		if not self.dirty:
			return 0

		gbase = self.update_main()

		# Like, actually build geometry now.
		# TODO: prep this list as a ctypes array
		geom = [0 for i in xrange(len(gbase) * 4 * (3 + 3))]
		p = 0
		for (dir, bx, by, bz, d) in gbase:
			faces = FACE_DICT[dir]
			for (x, y, z) in faces:
				geom[p + 0] = bx+x
				geom[p + 1] = -(bz+y)
				geom[p + 2] = -(by+z)
				geom[p + 3] = d[2]
				geom[p + 4] = d[1]
				geom[p + 5] = d[0]
				p += 6

		# All done!
		self.geom = geom
		self.dirty = False
		return 1


class Map(raw_pure.Map):
	chunk_loader = Chunk

	def load_vent(self, fp, x, y):
		# Now, this is a vxl loader which stores the pillars in VXL format.
		# It's a bit simpler, but at the cost of some of the other stuff being more complex.
		if x == 0:
			print "%i/%i" % (y+1, 512)

		l = ""
		while True:
			sa = fp.read(4)
			(d, s, e, a) = (ord(c) for c in sa)
			l += sa

			if d == 0:
				l += fp.read(4 * (e-s+1))
				break
			else:
				l += fp.read(4 * (d-1))

		nl = (c_ubyte * len(l))(*(ord(c) for c in l))
		#nl = tuple(ord(c) for c in l)

		return nl

	def get_block_main(self, bx, by, bz):
		ord = lambda c : c
		l = self.data[by][bx]
		p = 0
		while True:
			# Before the start, this is air.
			if bz < ord(l[p+1]):
				return None

			# Before and including the end, this is the visible floor.
			if bz <= ord(l[p+2]):
				ci = 4*(bz - ord(l[p+1]))
				return l[p+ci:p+ci+3]

			# If this is the end, it's hidden solid.
			if l[p] == "\x00":
				return True

			# There's only so much visible ceiling.
			# (D-1) - (E-S+1)
			crun = (ord(l[p])-1) - (ord(l[p+2])-ord(l[p+1])+1)

			# But we must move on to find out where it actually ends.
			p += 4*ord(l[p])

			# And now we can find out if we're in the hidden solid,
			if bz < ord(l[p+3]) - crun:
				return True

			# or in the visible ceiling.
			if bz < ord(l[p+3]):
				ci = 4*(bz - ord(l[p+3])) # This is not a mistake. We go backwards through the array.
				return l[p+ci:p+ci+3]

			# And once again, from the top.

	def set_block(self, bx, by, bz, v):
		bx %= 512
		by %= 512

		# TODO. This isn't actually going to work just yet.

		# Convert this and its neighbours to raw.
		l = self.to_raw(self, bx, by)
		lpx = self.to_raw(self, bx+1, by)
		lpy = self.to_raw(self, bx, by+1)
		lnx = self.to_raw(self, bx-1, by)
		lny = self.to_raw(self, bx, by-1)

		# Convert these back from raw.
		# Not done.

