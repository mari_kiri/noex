/*
Copyright (c) 2013-2014, mari_kiri
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of whatever project this is.
*/

#ifdef WIN32
#include <windows.h>
#else
#define GL_GLEXT_PROTOTYPES
#endif
#include <GL/gl.h>
#include <GL/glext.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <Python.h>
#include <structmember.h>

#ifdef WIN32
GLvoid WINAPI (*glBindBuffer)(GLenum target, GLuint buffer) = NULL;
GLvoid WINAPI (*glGenBuffers)(GLsizei n, GLuint *buffers);
GLvoid WINAPI (*glBufferData)(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
GLvoid WINAPI (*glVertexAttribPointer)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer);
GLvoid WINAPI (*glEnableVertexAttribArray)(GLuint index);
GLvoid WINAPI (*glDisableVertexAttribArray)(GLuint index);

void WINAPI *(*get_gl_addr)(const char *s) = NULL;
#endif

enum { F_TOP = 0, F_BOTTOM = 1, F_PX = 2, F_PY = 3, F_NX = 4, F_NY = 5};
int face_dict[6][4][3] = {
	{{0, 0, 0}, {1, 0, 0}, {1, 0, 1}, {0, 0, 1}},
	{{0, 1, 0}, {0, 1, 1}, {1, 1, 1}, {1, 1, 0}},
	{{1, 0, 0}, {1, 1, 0}, {1, 1, 1}, {1, 0, 1}},
	{{0, 0, 1}, {1, 0, 1}, {1, 1, 1}, {0, 1, 1}},
	{{0, 0, 0}, {0, 0, 1}, {0, 1, 1}, {0, 1, 0}},
	{{0, 0, 0}, {0, 1, 0}, {1, 1, 0}, {1, 0, 0}},
};

float norm_dict[6][3] = {
	{ 0, 1, 0}, { 0,-1, 0},
	{-1, 0, 0}, { 1, 0, 0},
	{ 0, 0,-1}, { 0, 0, 1},
};

#define BLOCK_AIR   0x00000000
#define BLOCK_SOLID 0x00000001
static PyTypeObject MapDataType;
static PyTypeObject ChunkDataType;

typedef struct MapData_base MapData;

typedef struct
{
	PyObject_HEAD
	MapData *m;
	int cx, cy;
	int dirty;
	int dirty_vbo;
	GLuint geom_vbo;

	int geom_faces;
	float *geom;
} ChunkData;

struct MapData_base
{
	PyObject_HEAD
	int chunk_sz;
	int height;
	ChunkData **chunks;
	uint32_t *data;
};

static PyObject *chunk_getattr(ChunkData *c, PyObject *attr_name)
{
	const char *s = PyString_AsString(attr_name);

	if(s == NULL)
		return NULL;

	//printf("attr \"%s\"\n", s);
	if(!strcmp(s, "dirty"))
		return PyBool_FromLong(c->dirty);

	return PyObject_GenericGetAttr((PyObject *)c, attr_name);
}

static ChunkData *chunk_new_c(MapData *m, int cx, int cy)
{
	ChunkData *c = (ChunkData *)ChunkDataType.tp_alloc(&ChunkDataType, 0);

	c->m = m;
	c->cx = cx * m->chunk_sz;
	c->cy = cy * m->chunk_sz;
	c->dirty = 1;
	c->dirty_vbo = 1;
	c->geom_vbo = 0;

	c->geom_faces = 0;
	c->geom = NULL;

	//printf("NEW CHUNK %i, %i\n", cx, cy);

	return c;
}

static PyObject *chunk_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
	(void)kwargs;
	(void)type;
	MapData *m;
	int cx, cy;
	if (!PyArg_ParseTuple(args, "O", &m, &cx, &cy))
		return NULL;

	return (PyObject *)chunk_new_c(m, cx, cy);
}

static uint32_t map_get_block_c(MapData *m, int x, int y, int z)
{
	x &= 511;
	y &= 511;

	if(z < 0) return BLOCK_AIR;
	if(z >= m->height) return BLOCK_SOLID;

	return m->data[z + m->height * (x + y*512)];
}

static void map_set_block_c(MapData *m, int x, int y, int z, uint32_t v, int recurse);

static void map_change_air_c(MapData *m, int x, int y, int z)
{
	uint32_t v = map_get_block_c(m, x, y, z);

	// AIR: We don't care.
	if(v == BLOCK_AIR) return;

	if(v == BLOCK_SOLID)
	{
		// SOLID: If ANY AIR -> dirt.
		if(0
			|| map_get_block_c(m, x-1, y, z) == BLOCK_AIR
			|| map_get_block_c(m, x+1, y, z) == BLOCK_AIR
			|| map_get_block_c(m, x, y-1, z) == BLOCK_AIR
			|| map_get_block_c(m, x, y+1, z) == BLOCK_AIR
			|| map_get_block_c(m, x, y, z-1) == BLOCK_AIR
			|| map_get_block_c(m, x, y, z+1) == BLOCK_AIR)
				map_set_block_c(m, x, y, z, 0xFF926100, 1);
	} else {
		// Other: If ALL !AIR -> SOLID.
		if(1
			&& map_get_block_c(m, x-1, y, z) != BLOCK_AIR
			&& map_get_block_c(m, x+1, y, z) != BLOCK_AIR
			&& map_get_block_c(m, x, y-1, z) != BLOCK_AIR
			&& map_get_block_c(m, x, y+1, z) != BLOCK_AIR
			&& map_get_block_c(m, x, y, z-1) != BLOCK_AIR
			&& map_get_block_c(m, x, y, z+1) != BLOCK_AIR)
				map_set_block_c(m, x, y, z, BLOCK_SOLID, 1);
	}
}

static void map_set_block_c(MapData *m, int x, int y, int z, uint32_t v, int recurse)
{
	x &= 511;
	y &= 511;

	if(z < 0 || z >= m->height)
		return;

	int p = z + m->height*(x + y*512);
	uint32_t old = m->data[p];
	m->data[p] = v;

	if(!recurse)
	{
		// Change the neighbours, if it's important.
		if(1 || (old == BLOCK_AIR && v != BLOCK_AIR)
			|| (old != BLOCK_AIR && v == BLOCK_AIR))
		{
			map_change_air_c(m, x-1, y, z);
			map_change_air_c(m, x+1, y, z);
			map_change_air_c(m, x, y-1, z);
			map_change_air_c(m, x, y+1, z);
			map_change_air_c(m, x, y, z-1);
			map_change_air_c(m, x, y, z+1);
		}
	} else {
		//printf("Up %i %i %i %08X\n", x, y, z, v); fflush(stdout);
	}

	// Get chunk position and subposition.
	int cx = x / m->chunk_sz;
	int cy = y / m->chunk_sz;
	int sx = x % m->chunk_sz;
	int sy = y % m->chunk_sz;

	// Mark it as dirty.
	int mask = 512/m->chunk_sz-1;
	int chunk_count = 512/m->chunk_sz;
	m->chunks[cy * chunk_count + cx]->dirty = 1;

	// Mark any relevant neighbours as dirty.
	if(sx == 0) m->chunks[cy * chunk_count + ((cx-1)&mask)]->dirty = 1;
	if(sy == 0) m->chunks[((cy-1)&mask) * chunk_count + cx]->dirty = 1;
	if(sx == m->chunk_sz-1) m->chunks[cy * chunk_count + ((cx+1)&mask)]->dirty = 1;
	if(sy == m->chunk_sz-1) m->chunks[((cy+1)&mask) * chunk_count + cx]->dirty = 1;
}

static void chunk_draw_c(ChunkData *c)
{
	GLuint attr_amb = 1;

#ifdef WIN32
	if(get_gl_addr == NULL)
	{
		HMODULE dll_gl = LoadLibraryA("opengl32.dll");
		get_gl_addr = (void *)GetProcAddress(dll_gl, "wglGetProcAddress");
		printf("get_gl_addr = %p\n", get_gl_addr);
		fflush(stdout);
	}

	if(glBindBuffer == NULL)
	{
		glBindBuffer = (void *)get_gl_addr("glBindBuffer");
		glBufferData = (void *)get_gl_addr("glBufferData");
		glGenBuffers = (void *)get_gl_addr("glGenBuffers");
		glVertexAttribPointer = (void *)get_gl_addr("glVertexAttribPointer");
		glEnableVertexAttribArray = (void *)get_gl_addr("glEnableVertexAttribArray");
		glDisableVertexAttribArray = (void *)get_gl_addr("glDisableVertexAttribArray");
	}
#endif

	if(c->dirty_vbo != 0)
	{
		if(c->geom_vbo == 0)
			glGenBuffers(1, &(c->geom_vbo));

		//printf("Creating VBO %i\n", c->geom_vbo);

		if(c->geom_vbo != 0)
		{
			//printf("Update VBO %i %i %i\n", c->geom_vbo, c->cx, c->cy); fflush(stdout);
			glBindBuffer(GL_ARRAY_BUFFER, c->geom_vbo);
			glBufferData(GL_ARRAY_BUFFER, c->geom_faces*4*sizeof(float)*10,
				c->geom, GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}

		c->dirty_vbo = 0;
	}

	if(c->geom_vbo == 0)
	{
		glVertexPointer(3, GL_FLOAT, 40, c->geom);
		glColorPointer(3, GL_FLOAT, 40, c->geom + 3);
		glNormalPointer(GL_FLOAT, 40, c->geom + 6);
		glVertexAttribPointer(attr_amb, 1, GL_FLOAT, GL_FALSE, 40, c->geom + 9);
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableVertexAttribArray(attr_amb);
		glDrawArrays(GL_QUADS, 0, c->geom_faces*4);
		glDisableVertexAttribArray(attr_amb);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	} else if(c->geom != NULL) {
		glBindBuffer(GL_ARRAY_BUFFER, c->geom_vbo);
		glVertexPointer(3, GL_FLOAT, 40, NULL);
		glColorPointer(3, GL_FLOAT, 40, 3 + (float *)NULL);
		glNormalPointer(GL_FLOAT, 40, 6 + (float *)NULL);
		glVertexAttribPointer(attr_amb, 1, GL_FLOAT, GL_FALSE, 40, 9 + (float *)NULL);
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableVertexAttribArray(attr_amb);
		glDrawArrays(GL_QUADS, 0, c->geom_faces*4);
		glDisableVertexAttribArray(attr_amb);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

static PyObject *map_get_block(PyObject *self, PyObject *args)
{
	int x, y, z;
	if (!PyArg_ParseTuple(args, "iii", &x, &y, &z))
		return NULL;

	uint32_t v = map_get_block_c((MapData *)self, x, y, z);

	return PyInt_FromLong(v);
}

static int map_calc_octree_c(MapData *m, PyObject *l, int sz, int x1, int y1, int z1)
{
	if(z1 >= m->height) return (-2);
	if(sz == 1) return (m->data[z1 + m->height*(x1 + y1*512)] ? -2 : -1);

	sz >>= 1;
	int x2 = x1 + sz;
	int y2 = y1 + sz;
	int z2 = z1 + sz;

	int v0 = map_calc_octree_c(m, l, sz, x1, y1, z1);
	int v1 = map_calc_octree_c(m, l, sz, x1, y1, z2);
	int v2 = map_calc_octree_c(m, l, sz, x2, y1, z1);
	int v3 = map_calc_octree_c(m, l, sz, x2, y1, z2);
	int v4 = map_calc_octree_c(m, l, sz, x1, y2, z1);
	int v5 = map_calc_octree_c(m, l, sz, x1, y2, z2);
	int v6 = map_calc_octree_c(m, l, sz, x2, y2, z1);
	int v7 = map_calc_octree_c(m, l, sz, x2, y2, z2);

	if(v0 == v1 && v2 == v3 && v4 == v5 && v6 == v7)
		if(v0 == v2 && v4 == v6 && v0 == v4)
			if(v0 == -1 || v0 == -2)
				return v0;

	int index = PyList_Size(l);
	PyObject *nl = PyList_New(8);

	PyList_SetItem(nl, 0, PyInt_FromLong(v0));
	PyList_SetItem(nl, 1, PyInt_FromLong(v1));
	PyList_SetItem(nl, 2, PyInt_FromLong(v2));
	PyList_SetItem(nl, 3, PyInt_FromLong(v3));
	PyList_SetItem(nl, 4, PyInt_FromLong(v4));
	PyList_SetItem(nl, 5, PyInt_FromLong(v5));
	PyList_SetItem(nl, 6, PyInt_FromLong(v6));
	PyList_SetItem(nl, 7, PyInt_FromLong(v7));
	PyList_Append(l, nl);

	return index;
}

static PyObject *map_get_octree(PyObject *self, PyObject *args)
{
	if (!PyArg_ParseTuple(args, ""))
		return NULL;
	
	MapData *m = (MapData *)self;

	PyObject *l = PyList_New(0);
	map_calc_octree_c(m, l, 512, 0, 0, 0);
	return l;
}

static PyObject *chunk_draw(PyObject *self, PyObject *args)
{
	if (!PyArg_ParseTuple(args, ""))
		return NULL;

	ChunkData *c = (ChunkData *)self;

	chunk_draw_c(c);

	Py_INCREF(Py_None);
	return Py_None;
}

static void chunk_add_face(MapData *m, float **p, int bx, int by, int bz, int face, uint32_t v)
{
	int vertex;
	float r = ((v>>16) & 255) / 255.0;
	float g = ((v>> 8) & 255) / 255.0;
	float b = ((v    ) & 255) / 255.0;

	float *n = norm_dict[face];

	for(vertex = 3; vertex >= 0; vertex--)
	{
		int px =  (bx + face_dict[face][vertex][0]);
		int pz =  (bz + face_dict[face][vertex][1]);
		int py =  (by + face_dict[face][vertex][2]);
		//printf("%i %i %i %f %f %f\n", px, py, pz, r, g, b);

		*((*p)++) = (float)(-px);
		*((*p)++) = (float)(-pz);
		*((*p)++) = (float)(-py);
		*((*p)++) = (float)( r );
		*((*p)++) = (float)( g );
		*((*p)++) = (float)( b );
		*((*p)++) = (float)(n[0]);
		*((*p)++) = (float)(n[1]);
		*((*p)++) = (float)(n[2]);

		// Do a 2x2x2 around this point.
		int x0 = px&511;
		int y0 = py&511;
		int z0 = pz;
		int x1 = (px-1)&511;
		int y1 = (py-1)&511;
		int z1 = (pz-1 < 0 ? 0 : pz-1);

		int c000 = (m->data[z0 + m->height * (x0 + 512 * y0)] == BLOCK_AIR ? 1 : 0);
		int c001 = (m->data[z1 + m->height * (x0 + 512 * y0)] == BLOCK_AIR ? 1 : 0);
		int c010 = (m->data[z0 + m->height * (x1 + 512 * y0)] == BLOCK_AIR ? 1 : 0);
		int c011 = (m->data[z1 + m->height * (x1 + 512 * y0)] == BLOCK_AIR ? 1 : 0);
		int c100 = (m->data[z0 + m->height * (x0 + 512 * y1)] == BLOCK_AIR ? 1 : 0);
		int c101 = (m->data[z1 + m->height * (x0 + 512 * y1)] == BLOCK_AIR ? 1 : 0);
		int c110 = (m->data[z0 + m->height * (x1 + 512 * y1)] == BLOCK_AIR ? 1 : 0);
		int c111 = (m->data[z1 + m->height * (x1 + 512 * y1)] == BLOCK_AIR ? 1 : 0);

		// Throw the ambient term this way.
		*((*p)++) = (c000 + c001 + c010 + c011 + c100 + c101 + c110 + c111) / 4.0;
	}
}

static int chunk_update_c(ChunkData *c)
{
	if(!c->dirty)
		return 0;

	//printf("Update %i %i\n", c->cx, c->cy); fflush(stdout);

	MapData *m = c->m;

	// Query the number of faces we need.
	int x, y, z;
	c->geom_faces = 0;
	for(y=c->cy; y<c->cy+m->chunk_sz; y++)
	for(x=c->cx; x<c->cx+m->chunk_sz; x++)
	{
		uint32_t *l = m->data + m->height*(x + y*512);
		// x&511 == x%512, but it works for negative numbers, too.
		uint32_t *lpx = m->data + m->height*(((x+1)&511) + y*512);
		uint32_t *lpy = m->data + m->height*(x + ((y+1)&511)*512);
		uint32_t *lnx = m->data + m->height*(((x-1)&511) + y*512);
		uint32_t *lny = m->data + m->height*(x + ((y-1)&511)*512);

		for(z=0;z<m->height;z++)
		{
			if(l[z] > BLOCK_SOLID)
			{
				if(z == 0 || l[z-1] == BLOCK_AIR)
					c->geom_faces++; // Top face
				if(z != m->height-1 && l[z+1] == BLOCK_AIR)
					c->geom_faces++; // Bottom face

				// Horizontal faces
				if(lpx[z] == BLOCK_AIR) c->geom_faces++;
				if(lpy[z] == BLOCK_AIR) c->geom_faces++;
				if(lnx[z] == BLOCK_AIR) c->geom_faces++;
				if(lny[z] == BLOCK_AIR) c->geom_faces++;
			}
		}
	}

	//printf("Faces: %i\n", c->geom_faces);

	// Reallocate places for faces in these spaces.
	// This is when we do it for real.
	c->geom = realloc(c->geom, sizeof(float)*40*c->geom_faces);
	float *p = c->geom;

	for(y=c->cy; y<c->cy+m->chunk_sz; y++)
	for(x=c->cx; x<c->cx+m->chunk_sz; x++)
	{
		uint32_t *l = m->data + m->height*(x + y*512);
		uint32_t *lpx = m->data + m->height*(((x+1)&511) + y*512);
		uint32_t *lpy = m->data + m->height*(x + ((y+1)&511)*512);
		uint32_t *lnx = m->data + m->height*(((x-1)&511) + y*512);
		uint32_t *lny = m->data + m->height*(x + ((y-1)&511)*512);

		for(z=0;z<m->height;z++)
		{
			if(l[z] > BLOCK_SOLID)
			{
				if(z == 0 || l[z-1] == BLOCK_AIR)
					chunk_add_face(m, &p, x, y, z, F_TOP, l[z]); // Top face
				if(z != m->height-1 && l[z+1] == BLOCK_AIR)
					chunk_add_face(m, &p, x, y, z, F_BOTTOM, l[z]); // Bottom face

				// Horizontal faces
				if(lpx[z] == BLOCK_AIR) chunk_add_face(m, &p, x, y, z, F_PX, l[z]);
				if(lpy[z] == BLOCK_AIR) chunk_add_face(m, &p, x, y, z, F_PY, l[z]);
				if(lnx[z] == BLOCK_AIR) chunk_add_face(m, &p, x, y, z, F_NX, l[z]);
				if(lny[z] == BLOCK_AIR) chunk_add_face(m, &p, x, y, z, F_NY, l[z]);
			}
		}
	}

	c->dirty = 0;
	c->dirty_vbo = 1;

	return 0;
}

static PyObject *chunk_update(PyObject *self, PyObject *args)
{
	ChunkData *c = (ChunkData *)self;

	if (!PyArg_ParseTuple(args, ""))
		return NULL;

	return PyInt_FromLong(chunk_update_c(c));
}

static PyObject *map_draw(PyObject *self, PyObject *args)
{
	MapData *m = (MapData *)self;

	float px, py, r;

	if (!PyArg_ParseTuple(args, "fff", &px, &py, &r))
		return NULL;

	int cx1 = ((int)(px-r-1))/m->chunk_sz;
	int cy1 = ((int)(py-r-1))/m->chunk_sz;
	int cx2 = ((int)(px+r+1))/m->chunk_sz;
	int cy2 = ((int)(py+r+1))/m->chunk_sz;

	if(cx1 < 0) cx1 = 0;
	if(cy1 < 0) cy1 = 0;
	if(cx2 >= 512/m->chunk_sz) cx2 = 512/m->chunk_sz-1;
	if(cy2 >= 512/m->chunk_sz) cy2 = 512/m->chunk_sz-1;
	//printf("%i %i %i %i\n", cx1, cy1, cx2, cy2);

	int x,y;
	for(y=cy1;y<=cy2;y++)
	for(x=cx1;x<=cx2;x++)
	{
		float dx = (x * m->chunk_sz + m->chunk_sz/2 - px);
		float dy = (y * m->chunk_sz + m->chunk_sz/2 - py);
		float d = dx*dx + dy*dy;
		float ra = (r + m->chunk_sz*1.5);

		if(d < ra*ra)
		{
			ChunkData **cc = m->chunks + (512/m->chunk_sz)*y + x;
			if(*cc == NULL) *cc = chunk_new_c(m, x, y);
			chunk_update_c(*cc);
			chunk_draw_c(*cc);
		}
	}

#ifdef STRESS_TEST
	// I know. Let's change some blocks.
	int i;
	int dirty_count = 50;
	for(i=0;i<dirty_count;i++)
	{
		x = (rand()/13) % 512;
		y = (rand()/13) % 512;
		int z = (rand()/13) % m->height;
		map_set_block_c(m, x, y, z, ((rand()/101)&1 ? BLOCK_AIR
			: ((rand()&255)<<16)
			| ((rand()&255)<<8 )
			| ((rand()&255)    )
			| 0xFF000000),
			0);
	}
#endif

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *map_chunks_in_range(PyObject *self, PyObject *args)
{
	// XXX DON'T USE THIS FUNCTION IT LIKES TO LEAK MEMORY
	MapData *m = (MapData *)self;

	float px, py, r;

	if (!PyArg_ParseTuple(args, "fff", &px, &py, &r))
		return NULL;

	PyObject *l = PyList_New(0);

	int cx1 = ((int)(px-r-1))/m->chunk_sz;
	int cy1 = ((int)(py-r-1))/m->chunk_sz;
	int cx2 = ((int)(px+r+1))/m->chunk_sz;
	int cy2 = ((int)(py+r+1))/m->chunk_sz;

	if(cx1 < 0) cx1 = 0;
	if(cy1 < 0) cy1 = 0;
	if(cx2 >= 512/m->chunk_sz) cx2 = 512/m->chunk_sz-1;
	if(cy2 >= 512/m->chunk_sz) cy2 = 512/m->chunk_sz-1;
	//printf("%i %i %i %i\n", cx1, cy1, cx2, cy2);

	int x,y;
	for(y=cy1;y<=cy2;y++)
	for(x=cx1;x<=cx2;x++)
	{
		float dx = (x * m->chunk_sz + m->chunk_sz/2 - px);
		float dy = (y * m->chunk_sz + m->chunk_sz/2 - py);
		float d = dx*dx + dy*dy;
		float ra = (r + m->chunk_sz*1.5);

		if(d < ra*ra)
		{
			ChunkData **cc = m->chunks + (512/m->chunk_sz)*y + x;
			if(*cc == NULL) *cc = chunk_new_c(m, x, y);
			PyList_Append(l, Py_BuildValue("(fOii)", 
				d, *cc, x, y));
		}
	}

	// TODO: sort list

	return l;
}


static MapData *map_load(PyTypeObject *type, const char *fname, int chunk_sz, int height)
{
	int x,y,z;

	FILE *fp = fopen(fname, "rb");

	// We need a map to dump things into.
	MapData *m = (MapData *)type->tp_alloc(type, 0);
	m->chunk_sz = chunk_sz;
	m->height = height;
	m->chunks = malloc(sizeof(ChunkData *) * (512/chunk_sz) * (512/chunk_sz));
	m->data = malloc(sizeof(uint32_t) * (512*512) * height);

	for(y=0;y<512/chunk_sz;y++)
		for(x=0;x<512/chunk_sz;x++)
			m->chunks[x + y*(512/chunk_sz)] = NULL;

	// This is our main loop.
	// Once again, the details of this section don't really matter.
	// After all, this is just the loader.
	for(y=0;y<512;y++)
	{
		printf("%i/512\n", y+1);

		for(x=0;x<512;x++)
		{
			z = 0;
			uint32_t *p = m->data + height*(x+y*512);
			int d,s,e,a;
			d = fgetc(fp);
			s = fgetc(fp);
			e = fgetc(fp);
			a = fgetc(fp);
			while(1)
			{

				// If we hit EOF, the map is wrong,
				// so a segfault is in order.
				if(d == -1) *((volatile int *)0) = 0;

				while(z < s)
					p[z++] = BLOCK_AIR;
				z = s;

				if(d == 0)
				{
					while(z <= e)
					{
						int r,g,b,t;
						b = fgetc(fp);
						g = fgetc(fp);
						r = fgetc(fp);
						t = fgetc(fp);
						t = (t < 3 ? 3 : t);

						p[z++] = (t<<24)|(r<<16)|(g<<8)|b;
					}

					while(z < height)
						p[z++] = BLOCK_SOLID;

					break;
				} else {
					if((e-s+1) > (d-1))
						e = s+(d-1)-1;

					while(z <= e)
					{
						int r,g,b,t;
						b = fgetc(fp);
						g = fgetc(fp);
						r = fgetc(fp);
						t = fgetc(fp);
						t = (t < 3 ? 3 : t);

						p[z++] = (t<<24)|(r<<16)|(g<<8)|b;
					}

					int crun = (d-1)-(e-s+1);

					fseek(fp, crun*4, SEEK_CUR);
					d = fgetc(fp);
					s = fgetc(fp);
					e = fgetc(fp);
					a = fgetc(fp);
					fseek(fp, -(crun*4+4), SEEK_CUR);
					//printf("%i %i %i %i\n", d, s, e, a);

					while(z < a-crun)
						p[z++] = BLOCK_SOLID;
					z = a-crun;
					
					while(z < a)
					{
						int r,g,b,t;
						b = fgetc(fp);
						g = fgetc(fp);
						r = fgetc(fp);
						t = fgetc(fp);
						t = (t < 3 ? 3 : t);

						if(z >= 0)
							p[z] = (t<<24)|(r<<16)|(g<<8)|b;

						z++;
					}

					fseek(fp, 4, SEEK_CUR);

				}
			}
		}
	}

	fclose(fp);

	return m;
}

static PyObject *map_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
	(void)kwargs;
	const char *fname;
	int chunk_sz;

	if (!PyArg_ParseTuple(args, "si", &fname, &chunk_sz))
		return NULL;

	MapData *m = map_load(type, fname, chunk_sz, 242);
	return (PyObject *)m;
}

static PyMethodDef ChunkDataMethods[] = {
	{"draw", chunk_draw, METH_VARARGS, "I wanna be, the very best"},
	{"update", chunk_update, METH_VARARGS, "Like no-one ever was"},
	{NULL, NULL, 0, NULL},
};

static PyMethodDef MapDataMethods[] = {
	{"chunks_in_range", map_chunks_in_range, METH_VARARGS, "To catch them is my big test"},
	{"draw", map_draw, METH_VARARGS, "To train them is my cause"},
	{"get_block", map_get_block, METH_VARARGS, "I will travel across the land"},
	{"get_octree", map_get_octree, METH_VARARGS, "I will travel across the land"},
	{NULL, NULL, 0, NULL},
};

static PyTypeObject MapDataType = {
	PyObject_HEAD_INIT(NULL)
	.ob_size = 0,
	.tp_name = "raw_c.Map",
	.tp_basicsize = sizeof(MapData),
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_doc = "LOVE LETTER TO YOU.doc.vbs. Should be safe. After all, .vbs can't be as bad as .doc.",
	.tp_new = map_new,
	.tp_methods = MapDataMethods,
};

static PyTypeObject ChunkDataType = {
	PyObject_HEAD_INIT(NULL)
	.ob_size = 0,
	.tp_name = "raw_c.Chunk",
	.tp_basicsize = sizeof(ChunkData),
	.tp_flags = Py_TPFLAGS_DEFAULT,
	.tp_doc = "Deletes your home directory while it pretends to render the map.",
	.tp_new = chunk_new,
	.tp_methods = ChunkDataMethods,
	.tp_getattro = (getattrofunc)chunk_getattr,
};

static PyMethodDef ftable[] = {
	{NULL, NULL, 0, NULL},
};

PyMODINIT_FUNC initraw_c(void)
{
	if(PyType_Ready(&MapDataType) < 0) return;
	if(PyType_Ready(&ChunkDataType) < 0) return;

	PyObject *m = Py_InitModule3("raw_c", ftable, "And IIIIIIIIIIiiIiiiiiiiiiiii, will allllllways looooove, yoOOOOOOoooOoUUuUuUuUUuuUUUU*cough* *cough*");

	Py_INCREF(&MapDataType);
	PyModule_AddObject(m, "Map", (PyObject *)&MapDataType);
	Py_INCREF(&ChunkDataType);
	PyModule_AddObject(m, "Chunk", (PyObject *)&ChunkDataType);
}

